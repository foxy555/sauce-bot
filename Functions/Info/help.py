import discord
from discord.ext import commands
helpEmbed = discord.Embed(title="Saucer bot",
                          description="All possible commands")
helpEmbed.add_field(name="-saucer help",
                    value="Shows this message", inline=False)
helpEmbed.add_field(name="-saucer start",
                    value="Starts reading author's messages from now on", inline=False)
helpEmbed.add_field(name="-saucer discard",
                    value="Stop and discard the data", inline=False)
helpEmbed.add_field(name="-saucer save *filename* download",
                    value="Stops receiving messages from author and dm author the file", inline=False)
helpEmbed.add_field(name="-saucer save *filename* send *channel*",
                    value="Stops receiving messages from author and send file to specified channel", inline=False)
helpEmbed.add_field(name="-saucer quality",
                    value="Shows the current quality of images", inline=False)
helpEmbed.add_field(name="-saucer quality *number*",
                    value="Changes the quality of images", inline=False)
helpEmbed.add_field(name="-saucer clear",
                    value="Clears all sessions (admins only)", inline=False)
helpEmbed.add_field(name="-saucer sessions",
                    value="Lists all sessions", inline=False)


class Help(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="help")
    async def printHelp(self, ctx):
        await ctx.send(embed=helpEmbed)


def setup(bot):
    bot.add_cog(Help(bot))
