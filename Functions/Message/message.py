import discord
from discord.ext import commands


class Message(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        await ctx.message.add_reaction("❌")
        await ctx.send("You did not send a valid command or something went terribly wrong\nUse -saucer help to see possible commands")

    @commands.Cog.listener()
    async def on_command_completion(self, ctx):
        await ctx.message.add_reaction("✅")


def setup(bot):
    bot.add_cog(Message(bot))
