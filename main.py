import discord
from discord.ext import commands
import os
import logging

client = commands.Bot(command_prefix="-saucer ", help_command=None)
cogs = ["Functions.Info.help",
        "Functions.Message.message",
        "Functions.Saucer.saucer"]


@client.event
async def on_ready():
    logging.info("bot ready!")
    await client.change_presence(activity=discord.Activity(type=discord.ActivityType.listening, name="-saucer start"))
    for cog in cogs:
        try:
            logging.info(f"Loading cog {cog}")
            client.load_extension(cog)
            logging.info(f"Loaded cog {cog}")
        except Exception as e:
            logging.error(f"Failed to load cog {cog} ")
            logging.error(e)

logger = logging.getLogger('discord')
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(
    filename='discord.log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter(
    '%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)
token = os.getenv("TOKEN")
if token == None:
    print("read token failed, attempting to read from .env file")
    f = open(".env", "r")
    token = f.read().split("=")[1]

client.run(token)
